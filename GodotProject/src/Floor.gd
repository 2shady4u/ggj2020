tool
extends Spatial

onready var _coral1_resource : Resource
onready var _coral2_resource : Resource
onready var _coral3_resource : Resource
onready var _coral4_resource : Resource

onready var _stacked_kelp_resource : Resource

onready var _kelp_dictionary : Dictionary
onready var _coral_dictionary : Dictionary

onready var _flora : Spatial = $Flora

const LIMIT_X : float = 200.0
const CORAL_LIMIT_Z : float = 25.0

const KELP_LOWER_Z : float = 50.0
const KELP_UPPER_Z : float = 200.0

func _ready():
	randomize()
	respawn_flora()

func respawn_flora():
	_coral1_resource = preload("res://src/flora/Coral1.tscn")
	_coral2_resource = preload("res://src/flora/Coral2.tscn")
	_coral3_resource = preload("res://src/flora/Coral3.tscn")
	_coral4_resource = preload("res://src/flora/Coral4.tscn")

	_stacked_kelp_resource = preload("res://src/flora/StackedKelp.tscn")

	_kelp_dictionary = {
		"resource": _stacked_kelp_resource,
		"min_height": 4,
		"max_height": 10,
		"spawn_chance": 0.75,
		"chances": 48
		}

	_coral_dictionary = {
	"coral1": {
		"resource": _coral1_resource,
		"spawn_chance": 0.75,
		"chances": 6
		},
	"coral2": {
		"resource": _coral2_resource,
		"spawn_chance": 0.5,
		"chances": 6
		},
	"coral3": {
		"resource": _coral3_resource,
		"spawn_chance": 0.25,
		"chances": 4
		},
	"coral4": {
		"resource": _coral4_resource,
		"spawn_chance": 0.25,
		"chances": 4
		}
	}
	
	for child in _flora.get_children():
		child.queue_free()

	for coral_id in _coral_dictionary:
		for i in range(0, _coral_dictionary[coral_id].chances):
			var roll : float = randf()
			if _coral_dictionary[coral_id].spawn_chance > roll:
				var coral_instance = _coral_dictionary[coral_id].resource.instance()
				_flora.add_child(coral_instance)
				coral_instance.set_owner(owner)
				coral_instance.translation.x = LIMIT_X*2*(randf()-0.5)
				coral_instance.translation.z = CORAL_LIMIT_Z*2*(randf()-0.5)
				coral_instance.rotation_degrees.y = 360*2*(randf()-0.5)

	for i in range(0, _kelp_dictionary.chances):
		var roll : float = randf()
		if _kelp_dictionary.spawn_chance > roll:
			var kelp_instance = _kelp_dictionary.resource.instance()
			_flora.add_child(kelp_instance)
			kelp_instance.set_owner(owner)
			# Find the kelp's height!!!
			kelp_instance._stack_kelp(randi() %(_kelp_dictionary.max_height-_kelp_dictionary.min_height) + _kelp_dictionary.min_height)
			kelp_instance.translation.x = LIMIT_X*2*(randf()-0.5)
			var z_position = (KELP_UPPER_Z- KELP_LOWER_Z)*2*(randf()-0.5)
			if z_position < 0:
				z_position -= KELP_LOWER_Z
			else:
				z_position += KELP_LOWER_Z
			kelp_instance.translation.z = z_position
#			coral_instance.rotation_degrees.y = 360*2*(randf()-0.5)
