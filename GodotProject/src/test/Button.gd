extends Spatial

signal pressed

var _disabled := false

func _on_Interactable_interacted() -> void:
	if _disabled: return
	
	emit_signal("pressed")
	$PressSound.play()

func enable() -> void:
	_disabled = false

func disable() -> void:
	_disabled = true
