extends Control

onready var _restart_button : Button = $VBoxContainer/RestartButton

func _ready():
	Flow.current_state = Flow.STATE.FAIL
	Composer.update_composer()

	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	var _succes : int = _restart_button.connect("pressed", self, "_on_restart_button_pressed")

func _on_restart_button_pressed() -> void:
	Flow.change_scene_to("game")

func _input(event : InputEvent):
	if event.is_action_pressed("ui_cancel"):
		Flow.change_scene_to("menu")
