extends Area
class_name Interactable

signal interacted
signal highlighted
signal unhighlighted

func interact() -> void:
	emit_signal("interacted")

func highlight() -> void:
	emit_signal("highlighted")

func unhighlight() -> void:
	emit_signal("unhighlighted")
