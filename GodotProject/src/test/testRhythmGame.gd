extends Control

onready var _low_button : TextureButton = $VBoxContainer/HBoxContainer/LeftButton
onready var _middle_button : TextureButton = $VBoxContainer/HBoxContainer/MiddleButton
onready var _high_button : TextureButton = $VBoxContainer/HBoxContainer/RightButton

onready var _button_dictionary : Dictionary = {"low": _low_button, 
												"middle": _middle_button, 
												"high": _high_button}

onready var _error_sound : AudioStreamPlayer = $ErrorSound
onready var _success_sound : AudioStreamPlayer = $SuccessSound
onready var _timer : Timer = $Timer

onready var _listen_button : Button = $VBoxContainer/ListenButton

const FAST_INTERPAUSE : float = 0.25
const SLOW_INTERPAUSE : float = 0.75

const FAST_TIMEOUT : float = 1.5
const SLOW_TIMEOUT : float = 2.5

var _is_expecting_correct_pattern : bool = false
var _is_speed_enabled : bool = true
var _has_failed : bool = false
var _current_pattern_index = 0
var _current_notes : Array = []
var _current_speed : Array = []

func _ready():
	randomize()
	
	_timer.one_shot = true
	
	var _success : bool = _timer.connect("timeout", self, "_on_timer_timeout")
	_success = _listen_button.connect("pressed", self, "_on_listen_button_pressed")

	for button_id in _button_dictionary.keys():
		_button_dictionary[button_id].connect("pressed", self, "_on_audio_button_pressed", [button_id])

func _on_listen_button_pressed():
	# Let's find some pattern!
	# It's always the sax in this case!
	_listen_button.disabled = true
	for button_id in _button_dictionary.keys():
		_button_dictionary[button_id].mouse_filter = MOUSE_FILTER_IGNORE
		_button_dictionary[button_id].toggle_mode = true

	if _current_notes.empty():
		var candidate_patterns : Array = Flow.pattern_dictionary["sax"]
		var chosen_index : int = randi() % candidate_patterns.size()
		_current_notes = candidate_patterns[chosen_index].notes
		_current_speed = candidate_patterns[chosen_index].speed
	_is_expecting_correct_pattern = true
	_current_pattern_index = 0

	var interpause : float = SLOW_INTERPAUSE
	var index : int = 0
	for button_id in _current_notes:
		if _is_speed_enabled:
			match _current_speed[index]:
				"fast":
					interpause = FAST_INTERPAUSE
				"slow":
					interpause = SLOW_INTERPAUSE
		_button_dictionary[button_id].pressed = true
		_button_dictionary[button_id].play_sound()
		yield(get_tree().create_timer(interpause), "timeout")
		_button_dictionary[button_id].pressed = false
		yield(get_tree().create_timer(interpause), "timeout")
		index += 1

	for button_id in _button_dictionary.keys():
		_button_dictionary[button_id].mouse_filter = MOUSE_FILTER_STOP
		_button_dictionary[button_id].toggle_mode = false
	_listen_button.disabled = false

func _on_audio_button_pressed(button_id : String):
	if _button_dictionary.has(button_id):
		_button_dictionary[button_id].play_sound()
		if _is_expecting_correct_pattern:
			if button_id != _current_notes[_current_pattern_index]:
				print("failed! (incorrect note)")
				_timer.stop()
				_current_pattern_index = 0
				_error_sound.playing = true
			else:
				print(_timer.time_left)
				_timer.stop()
				_current_pattern_index += 1
				if _current_pattern_index >= _current_notes.size():
					_is_expecting_correct_pattern = false
					_current_notes.clear()
					_current_speed.clear()
					_success_sound.playing = true
				elif _is_speed_enabled:
					match _current_speed[_current_pattern_index-1]:
						"fast":
							_timer.start(FAST_TIMEOUT)
						"slow":
							_timer.start(SLOW_TIMEOUT)
	else:
		push_error("Button dictionary did not contain key {}".format([]))

func _on_timer_timeout():
	_current_pattern_index = 0
	print("failed! (too slow, timed out)")
	_error_sound.playing = true
