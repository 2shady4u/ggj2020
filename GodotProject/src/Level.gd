extends Spatial

func _ready() -> void:
	pass

func _connect_signals() -> void:
	for instrument in $Instruments.get_children():
		# warning-ignore:return_value_discarded
		(instrument as Instrument).connect("pattern_completed", self, "_on_Instrument_pattern_completed")

func _on_Instrument_pattern_completed() -> void:
	pass
