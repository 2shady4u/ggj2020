extends TextureButton

onready var _audio_stream : AudioStreamPlayer = $AudioStreamPlayer

func play_sound():
	_audio_stream.playing = true
