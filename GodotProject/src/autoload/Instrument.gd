extends Node

onready var _good_audio : AudioStreamPlayer3D = $Good
onready var _bad_audio : AudioStreamPlayer3D = $Bad
onready var _debug_cube : MeshInstance = $DebugCube
onready var _tween : Tween = $Tween

var is_instrument_broken : bool = false
var instrument_broken_level : float = 6

const DURATION : float = 1.0

func _ready():
	var _succes : bool = _tween.connect("tween_all_completed", self, "_on_tween_all_completed")
	_good_audio.unit_size = 10
	_bad_audio.unit_size = 10

func set_instrument_position(new_position : Vector3) -> void:
	_good_audio.translation = new_position
	_bad_audio.translation = new_position
	_debug_cube.translation = new_position + Vector3(0, 1, 0)

func set_instrument_status(new_value : bool) -> void:
	if new_value:
		_good_audio.playing = true
		_bad_audio.unit_db = -100
		_bad_audio.playing = true
	else:
		_good_audio.playing = false
		_bad_audio.playing = false

func set_instrument_broken_status(new_value : bool) -> void:
	if is_instrument_broken != new_value:
		is_instrument_broken = new_value
		if _bad_audio.unit_db == -100:
			var _succes : bool = _tween.interpolate_property(_bad_audio, "unit_db", -100, instrument_broken_level, DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
			_succes = _succes and _tween.interpolate_property(_good_audio, "unit_db", 0, -100, DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
			_succes = _succes and _tween.start()
		else:
			var _succes : bool = _tween.interpolate_property(_good_audio, "unit_db", -100, 0, DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
			_succes = _succes and _tween.interpolate_property(_bad_audio, "unit_db", instrument_broken_level, -100, DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
			_succes = _succes and _tween.start()
	else:
		return

func _on_tween_all_completed() -> void:
	print("done?")
