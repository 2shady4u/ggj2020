tool
extends Spatial

onready var _floor_resource : Resource
onready var _floors : Spatial

const SPEED : float = 1.0
const FLOOR_WIDTH : float = 400.0
const NUMBER_OF_FLOORS : int = 5

export var respawn : bool = false setget _set_respawn

func _ready():
	_set_respawn()

func _set_respawn(_new_value : bool = false):
	_floor_resource = preload("res://src/Floor.tscn")
	_floors = $Floors
	set_physics_process(true)

	var middle_number : int = floor(NUMBER_OF_FLOORS/2.0)

	for child in _floors.get_children():
		child.queue_free()

	for i in range(0, NUMBER_OF_FLOORS):
		var floor_instance = _floor_resource.instance()
		_floors.add_child(floor_instance)
		floor_instance.set_owner(owner)
		floor_instance.translation.x = (i-middle_number)*FLOOR_WIDTH

func _physics_process(_delta : float):
	for child in _floors.get_children():
		child.translation.x += SPEED#/_delta*ProjectSettings.get("physics/common/physics_fps")
		if child.translation.x > 800:
			child.translation.x -= 1600
