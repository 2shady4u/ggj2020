extends Node2D

onready var _tween : Tween = $Tween
onready var _sprite : Sprite = $Sprite

const X_START : int = 1024/3
const X_END : int = 2*1024/3
const DURATION : float = 2.0 

func _ready():
	var _succes : bool = _tween.interpolate_method(self, "move_sprite", 0.0, 1.0, DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	_tween.repeat = true
	_succes = _succes and _tween.start()

func move_sprite(value: float) -> void:
	_sprite.position.x = (1+cos(2*PI*value))*(X_END-X_START)/2 + X_START
