extends Spatial

onready var _mesh : MeshInstance = $Mesh
onready var _highlight_mesh : MeshInstance = $Mesh/HighlightMesh

var _active := true
const COLOR_ACTIVE := Color.green
const COLOR_INACTIVE := Color.red

export(String, "sax", "percussion", "keyboard", "harp") var _instrument_id : String = "sax"

func _ready() -> void:
	_mesh.material_override = _mesh.material_override.duplicate()
	_set_active()
	unhighlight()

	Composer.set_instrument_position(_instrument_id, translation)
	Composer.set_instrument_broken_status(_instrument_id, !_active)

func _set_active(value : bool = _active) -> void:
	_active = value
	
	if _active:
		_set_mesh_color(COLOR_ACTIVE)
	else:
		_set_mesh_color(COLOR_INACTIVE)

func toggle_active() -> void:
	_set_active(!_active)
	Composer.set_instrument_broken_status(_instrument_id, !_active)

func highlight() -> void:
	_highlight_mesh.visible = true

func unhighlight() -> void:
	_highlight_mesh.visible = false

func _on_Interactable_interacted() -> void:
	toggle_active()

func _set_mesh_color(color : Color) -> void:
	_mesh.material_override.albedo_color = color

func _on_Interactable_highlighted() -> void:
	highlight()

func _on_Interactable_unhighlighted() -> void:
	unhighlight()
