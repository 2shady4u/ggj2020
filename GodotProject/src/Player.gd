extends KinematicBody

onready var _pivot = $Pivot
onready var _camera = $Pivot/Camera
onready var _interact_ray = $Pivot/Camera/InteractRay

const GRAVITY := -30.0
const WALK_ACCELERATION := 1.5
const RUN_ACCELERATION := 2.5
const MOVE_DECELERATION := 1.0
const MAX_WALK_SPEED := 8.0
const MAX_RUN_SPEED := 14.0
const MOUSE_SENSITIVITY := 0.004
const INTERACT_DISTANCE := 2.0

var _velocity = Vector3()
var _controllable := true

var _last_interactable : Interactable = null

func _ready():
	_interact_ray.cast_to.z = -INTERACT_DISTANCE

func _unhandled_input(event):
	if _controllable:
		_process_mouse_input(event)

func _physics_process(delta):
	if _controllable:
		_process_horizontal_movement(delta)
	_apply_gravity(delta)
	_move()
	_process_interact()

func _process_mouse_input(event : InputEvent) -> void:
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotate_y(-event.relative.x * MOUSE_SENSITIVITY)
		_pivot.rotate_x(-event.relative.y * MOUSE_SENSITIVITY)
		_pivot.rotation.x = clamp(_pivot.rotation.x, -1.2, 1.2)

# warning-ignore:unused_argument
func _process_horizontal_movement(delta : float) -> void:
	# get input
	var direction : Vector3 = _get_horizontal_movement_direction()
	var acceleration : float = _get_horizontal_movement_acceleration()
	
	var horizontal_velocity := Vector2(_velocity.x, _velocity.z)
	
	# move
	horizontal_velocity.x += direction.x * acceleration
	horizontal_velocity.y += direction.z * acceleration
	
	# decelerate
	if horizontal_velocity.length() < MOVE_DECELERATION:
		horizontal_velocity = Vector2.ZERO
	else:
		horizontal_velocity = horizontal_velocity.normalized() * (horizontal_velocity.length() - MOVE_DECELERATION)
	
	# limit max speed
	var max_speed = _get_max_move_speed()
	horizontal_velocity = horizontal_velocity.clamped(max_speed)
	
	# add to main velocity
	_velocity.x = horizontal_velocity.x
	_velocity.z = horizontal_velocity.y

func _move() -> void:
	_velocity = move_and_slide(_velocity, Vector3.UP, true)

func _get_horizontal_movement_acceleration() -> float:
	if Input.is_action_pressed("sprint"):
		return RUN_ACCELERATION
	else:
		return WALK_ACCELERATION

func _get_max_move_speed() -> float:
	if Input.is_action_pressed("sprint"):
		return MAX_RUN_SPEED
	else:
		return MAX_WALK_SPEED

func _get_horizontal_movement_direction() -> Vector3:
	var input_direction := Vector3()
	
	if Input.is_action_pressed("move_forward"):
		input_direction -= _camera.global_transform.basis.z
	if Input.is_action_pressed("move_back"):
		input_direction += _camera.global_transform.basis.z
	if Input.is_action_pressed("move_left"):
		input_direction -= _camera.global_transform.basis.x
	if Input.is_action_pressed("move_right"):
		input_direction += _camera.global_transform.basis.x
	
	input_direction.y = 0
	return input_direction.normalized()

func _apply_gravity(delta : float) -> void:
	_velocity.y += GRAVITY * delta

func _process_highlight() -> void:
	var collider : CollisionObject = _interact_ray.get_collider()
	if collider != null and collider.is_in_group("interactables"):
		(collider as Interactable).highlight()

func _process_interact() -> void:
	var _new_collider : CollisionObject = _interact_ray.get_collider()

	# highlight and interact with new collider
	if _new_collider != null and _new_collider.is_in_group("interactables"):
		(_new_collider as Interactable).highlight()
		if Input.is_action_just_pressed("interact"):	
			(_new_collider as Interactable).interact()
	
	# update last collider
	if _last_interactable != _new_collider and _last_interactable != null:
		(_last_interactable as Interactable).unhighlight()
		_last_interactable = null
	if _new_collider is Interactable:
		_last_interactable = _new_collider
