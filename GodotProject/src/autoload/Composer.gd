extends Node

var is_harp_broken : bool = false
var is_percussion_broken : bool = false
var is_sax_broken : bool = false
var is_keyboard_broken : bool = false

onready var _ambiance_sound : AudioStreamPlayer = $Ambiance
onready var _menu_sound : AudioStreamPlayer = $MenuSound
onready var _fail_sound : AudioStreamPlayer = $FailSound
onready var _instrument_dictionary : Dictionary = {"harp" : $Harp, 
											"percussion": $Percussion, 
											"sax": $Sax, 
											"keyboard": $Keyboard}

func _ready():
# warning-ignore:return_value_discarded
	_fail_sound.connect("finished", self, "on_fail_sound_finished")

func update_composer(_is_new_instance : bool = false):
	match Flow.current_state:
		Flow.STATE.GAME:
			if _is_new_instance:
				_ambiance_sound.playing = true
			_menu_sound.stream_paused  = true
			_ambiance_sound.stream_paused = false
			for instrument_id in _instrument_dictionary.keys():
				_instrument_dictionary[instrument_id].set_instrument_status(true)
		Flow.STATE.MENU, Flow.STATE.FAIL:
			if Flow.current_state == Flow.STATE.FAIL:
				_fail_sound.playing = true
			else:
				_menu_sound.stream_paused  = false
			_ambiance_sound.stream_paused = true
			for instrument_id in _instrument_dictionary.keys():
				_instrument_dictionary[instrument_id].set_instrument_status(false)

func set_instrument_position(instrument_id : String, new_position : Vector3):
	if _instrument_dictionary.has(instrument_id):
		_instrument_dictionary[instrument_id].set_instrument_position(new_position)
	else:
		push_error("Instrument with ID '{0}' was not recognized!".format([instrument_id]))

func set_instrument_broken_status(instrument_id : String, new_value: bool):
	if _instrument_dictionary.has(instrument_id):
		_instrument_dictionary[instrument_id].set_instrument_broken_status(new_value)
	else:
		push_error("Instrument with ID '{0}' was not recognized!".format([instrument_id]))

func get_instrument_broken_status(instrument_id : String) -> bool:
	if _instrument_dictionary.has(instrument_id):
		return _instrument_dictionary[instrument_id].is_instrument_broken
	else:
		push_error("Instrument with ID '{0}' was not recognized!".format([instrument_id]))
	return false

func on_fail_sound_finished():
	_menu_sound.playing = true
	_menu_sound.stream_paused = false
