extends Spatial
class_name InstrumentElement

signal interacted(index)

onready var _note = $Note

export var index := -1
var disabled := false

func _on_Interactable_interacted() -> void:
	if disabled: return
	play()
	emit_signal("interacted", index)

func enable() -> void:
	disabled = false

func disable() -> void:
	disabled = true

func play() -> void:
	_note.play()
