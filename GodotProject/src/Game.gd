extends Control

#TODO: Processes might not be properly paused!!!
var is_paused : bool = false

func _notification(what):
	## Updates the game when entering the scenetree!
	if is_inside_tree() and what == NOTIFICATION_ENTER_TREE:
		update_game()

func update_game() -> void:
	Flow.reset()
	Flow.current_state = Flow.STATE.GAME
	Composer.update_composer()

	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event : InputEvent):
	if event.is_action_pressed("ui_cancel"):
		# save scene and remove it from the tree
		Flow.current_saved_game = self
		get_tree().get_root().remove_child(self)
		Flow.change_scene_to("menu")
