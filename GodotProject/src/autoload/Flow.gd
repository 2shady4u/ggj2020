extends Node

onready var _menu_resource = preload("res://src/Menu.tscn")
onready var _game_resource = preload("res://src/Game.tscn")
onready var _fail_resource = preload("res://src/Fail.tscn")
onready var _resource_dictionary : Dictionary = {"menu" : _menu_resource, 
												"game": _game_resource,
												"fail": _fail_resource,
												"saved_game": current_saved_game}

enum STATE {MENU, GAME, FAIL}
var current_state : int = STATE.GAME
enum DIFFICULTY {EASY, INTERMEDIATE, HARD}
var current_difficulty : int = DIFFICULTY.EASY
var current_saved_game : Node = null
var current_water_level : float = 0.0 #Water level between 0.0 and 1.0
var pattern_dictionary : Dictionary = {}

var water_level_mesh : Spatial = null

const PATTERNS_PATH : String = "res://data/patterns.json"

func _ready():
	_load_patterns()

func _input(event : InputEvent) -> void:
	_process_debug_controls(event)

func reset():
	current_water_level = 0.0

func _process_debug_controls(event : InputEvent) -> void:
	if event.is_action_pressed("debug_reset"):
		# warning-ignore:return_value_discarded
		get_tree().reload_current_scene()
	if event.is_action_pressed("debug_fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
	if event.is_action_pressed("increase_water_level"):
		current_water_level += 0.1
		if current_water_level >= 1.0:
			current_water_level = 1.0
			# Wipe the saved scene!
			if current_saved_game != null:
				current_saved_game.queue_free()
				current_saved_game = null
			Flow.change_scene_to("fail")
		if water_level_mesh != null and is_instance_valid(water_level_mesh):
			water_level_mesh.update_water_level()
	if event.is_action_pressed("decrease_water_level"):
		current_water_level -= 0.1
		if current_water_level < 0.0:
			current_water_level = 0.0
		if water_level_mesh != null:
			water_level_mesh.update_water_level()

func change_scene_to(resource_string : String) -> void:
	if _resource_dictionary.has(resource_string):
		var target_resource = _resource_dictionary[resource_string]
		var error : int = get_tree().change_scene_to(target_resource)
		if error != OK:
			push_error("Failed to change scene to '{0}'.".format([resource_string]))
		else:
			print("Succesfully changed scene to '{0}'.".format([resource_string]))
	else:
		push_error("Requested scene '{0}' was not recognized... ignoring call for changing scene.".format([resource_string]))

func _load_patterns() -> void:
	var file : File = File.new()
	var error : int = file.open(PATTERNS_PATH, File.READ)
	if error == OK:
		pattern_dictionary = parse_json(file.get_as_text())
		file.close()
		print("Succesfully loaded patterns from '{0}'.".format([PATTERNS_PATH]))
	else:
		push_error("Failed to open the patterns-JSON at '{0}' for loading purposes.".format([PATTERNS_PATH]))

	if pattern_dictionary == null:
		push_error("Detected null-value in patterns-JSON at {0}.".format([PATTERNS_PATH]))
