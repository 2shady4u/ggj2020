tool
extends Spatial

export var stacking_height : int = 3 setget _stack_kelp

const kelp_height : float = 3.0

onready var _kelp_resource : Resource = preload("res://src/flora/Kelp.tscn")

func _ready():
	_stack_kelp()

func _stack_kelp(new_value : int = stacking_height):
	_kelp_resource = preload("res://src/flora/Kelp.tscn")
	stacking_height = new_value

	for child in get_children():
		child.queue_free()

	for i in range(0, stacking_height):
		var kelp_instance = _kelp_resource.instance()
		add_child(kelp_instance)
		kelp_instance.set_owner(get_owner())
		
		kelp_instance.translation.y = i*kelp_height
