extends Spatial
class_name Instrument

signal pattern_completed

const SCENE_NOTE_PARTICLES = preload("res://src/particles/SingleNote.tscn")

onready var _answer_timer = $AnswerTimer
onready var _play_back_timer = $PlayBackTimer
onready var _play_back_button = $PlayBackButton

const FAST_TIMEOUT : float = 1.5
const SLOW_TIMEOUT : float = 2.5

const DEFAULT_INTERPAUSE := 0.75
const SPEED_TO_INTERPAUSE := {
	"fast": 0.25,
	"slow": 0.75,
}

export(String, "sax", "percussion", "keyboard", "harp") var _instrument_id : String = "sax"

var _patterns := []
var _is_expecting_correct_pattern : bool = false
var _is_speed_enabled : bool = true
var _has_failed : bool = false
var _current_pattern_index = 0
var _current_notes : Array = []
var _current_speed : Array = []

func _ready() -> void:
	randomize()
	_setup_elements()
	Composer.set_instrument_position(_instrument_id, translation)
	_patterns = Flow.pattern_dictionary[_instrument_id]

func play_back() -> void:
	_play_back_button.disable()
	_disable_elements()
	if not _is_expecting_correct_pattern:
		_get_random_pattern()
	_current_pattern_index = 0
	_advance_play_back_if_not_done()

func _advance_play_back_if_not_done() -> void:
	if _current_pattern_index >= _current_notes.size():
		_enable_elements()
		_play_back_button.enable()
		_is_expecting_correct_pattern = true
		_current_pattern_index = 0
		$PlayBackAP.play("release")
		return
	
	var interpause : float = DEFAULT_INTERPAUSE
	if _is_speed_enabled:
		interpause = SPEED_TO_INTERPAUSE[_current_speed[_current_pattern_index]]
	var element_index = int(_current_notes[_current_pattern_index])
	
	var element : InstrumentElement = _get_element(element_index)
	element.play()
	if element.has_node("AP"):
		element.get_node("AP").stop()
		element.get_node("AP").play("interact")
	_create_note_particles(1)
	
	_current_pattern_index += 1
	_play_back_timer.start(interpause)

func _setup_elements() -> void:
	for i in _get_elements_count():
		var element : InstrumentElement = _get_element(i)
		element.index = i
		# warning-ignore:return_value_discarded
		element.connect("interacted", self, "_on_element_interacted")

func _get_random_pattern() -> void:
	var random_index := randi() % _patterns.size()
	_current_notes = _patterns[random_index].notes
	_current_speed = _patterns[random_index].speed

func _disable_elements() -> void:
	for element in _get_elements():
		element.disable()

func _enable_elements() -> void:
	for element in _get_elements():
		element.enable()

func _get_element(index : int) -> InstrumentElement:
	return $Elements.get_child(index) as InstrumentElement

func _get_elements() -> Array:
	return $Elements.get_children()

func _get_elements_count() -> int:
	return $Elements.get_child_count()

func _on_element_interacted(element_index : int) -> void:
	_get_element(element_index).play()
	_create_note_particles(1)
	if _get_element(element_index).has_node("AP"):
		_get_element(element_index).get_node("AP").stop()
		_get_element(element_index).get_node("AP").play("interact")
	
	if _is_expecting_correct_pattern:
		_answer_timer.stop()
		_play_back_timer.stop()
		if int(_current_notes[_current_pattern_index]) != element_index:
			_current_pattern_index = 0
			_is_expecting_correct_pattern = false
			$WrongSound.play()
		else:
			_current_pattern_index += 1
			if _current_pattern_index >= _current_notes.size():
				_is_expecting_correct_pattern = false
				$CorrectSound.play()
				_create_note_particles(10)
				_current_pattern_index = 0
				emit_signal("pattern_completed")
			elif _is_speed_enabled:
				match _current_speed[_current_pattern_index - 1]:
					"fast":
						_answer_timer.start(FAST_TIMEOUT)
					"slow":
						_answer_timer.start(SLOW_TIMEOUT)

func _on_PlayBackTimer_timeout() -> void:
	_advance_play_back_if_not_done()

func _on_PlayBackButton_pressed() -> void:
	play_back()
	$PlayBackAP.play("press")

func _create_note_particles(count : int) -> void:
	while count > 0:
		var note = SCENE_NOTE_PARTICLES.instance()
		add_child(note)
		note.global_transform = $NoteParticlesPosition.global_transform
		count -= 1
