extends Control

onready var _new_button : Button = $VBoxContainer/NewButton
onready var _continue_button : Button = $VBoxContainer/ContinueButton
onready var _quit_button : Button = $VBoxContainer/QuitButton
onready var _h_slider : HSlider = $VBoxContainer/HSlider
onready var _volume_label : HSlider = $VBoxContainer/HBoxContainer/VolumeLabel

func _ready():
	Flow.current_state = Flow.STATE.MENU
	Composer.update_composer()

	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	var _succes : int = _new_button.connect("pressed", self, "_on_new_button_pressed")
	_succes = _continue_button.connect("pressed", self, "_on_continue_button_pressed")
	_succes = _quit_button.connect("pressed", self, "_on_quit_button_pressed")
	_succes = _h_slider.connect("value_changed", self, "_on_volume_slider_value_changed")

	#Init the sliders correctly!
	_on_volume_slider_value_changed(AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Master")))

func _on_new_button_pressed() -> void:
	Flow.change_scene_to("game")

func _on_continue_button_pressed() -> void:
	if Flow.current_saved_game != null:
		# free temporary scene
		get_tree().get_current_scene().queue_free()
		# add saved scene back to the tree
		get_tree().get_root().add_child(Flow.current_saved_game)
	else:
		Flow.change_scene_to("game")

func _on_quit_button_pressed():
	get_tree().quit()

func _on_volume_slider_value_changed(new_value : float) -> void:
	_h_slider.value = new_value
	_volume_label.text = "({0} dB)".format([new_value])

	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), new_value)
