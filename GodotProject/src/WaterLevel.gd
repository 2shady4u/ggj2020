extends Spatial

const MINIMUM_Y : float = -8.0
const MAXIMUM_Y : float = 5.0

onready var _tween : Tween = $Tween

func _ready():
	Flow.water_level_mesh = self
	update_water_level()

func update_water_level():
	var duration : float = 1.0
	# 0 is the MINIMUM_Y and 1.0 is the MAXIMUM_Y
	var future_y : float = MAXIMUM_Y*Flow.current_water_level + MINIMUM_Y*(1-Flow.current_water_level)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(self, "translation:y", translation.y, future_y, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.start()
